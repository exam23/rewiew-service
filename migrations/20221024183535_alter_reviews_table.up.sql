ALTER TABLE rewiews DROP COLUMN created_at;
ALTER TABLE rewiews DROP COLUMN updated_at;
ALTER TABLE rewiews DROP COLUMN deleted_at;

ALTER TABLE rewiews ADD COLUMN created_at TIMESTAMPTZ;
ALTER TABLE rewiews ALTER COLUMN created_at SET DEFAULT now();

ALTER TABLE rewiews ADD COLUMN updated_at TIMESTAMPTZ;
ALTER TABLE rewiews ALTER COLUMN updated_at SET DEFAULT now();

ALTER TABLE rewiews ADD COLUMN deleted_at TIMESTAMPTZ;