package storage

import (
	"github.com/jmoiron/sqlx"
	"gitlab.com/exam23/rewiew-service/storage/postgres"
	"gitlab.com/exam23/rewiew-service/storage/repo"
)

type IStorage interface {
	Rewiew() repo.RewiewStorageI
}

type StoragePg struct {
	Db       *sqlx.DB
	userRepo repo.RewiewStorageI
}

// NewStoragePg
func NewStoragePg(db *sqlx.DB) *StoragePg {
	return &StoragePg{
		Db:       db,
		userRepo: postgres.NewRewiewRepo(db),
	}
}

func (s StoragePg) Rewiew() repo.RewiewStorageI {
	return s.userRepo
}
