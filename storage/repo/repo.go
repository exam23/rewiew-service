package repo

import rs "gitlab.com/exam23/rewiew-service/genproto/rewiew"

type RewiewStorageI interface {
	Create(rs.RewiewReq) (*rs.RewiewRes, error)
	GetRewiewByPostId(rs.Id) (*rs.GetRewiewsRes, error)
	Update(rs.RewiewRes) (*rs.RewiewRes, error)
	Delete(rs.Id) error
	GetTop10Postids() (*rs.TopPosts, error)
	DeleteByCustomerId(req rs.Id) error
	DeleteByPostId(req rs.Id) error
}
