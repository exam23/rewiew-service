package postgres

import (
	"github.com/jmoiron/sqlx"
)

type RewiewRepo struct {
	Db *sqlx.DB
}

func NewRewiewRepo(db *sqlx.DB) *RewiewRepo {
	return &RewiewRepo{Db: db}
}