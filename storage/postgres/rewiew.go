package postgres

import (
	rs "gitlab.com/exam23/rewiew-service/genproto/rewiew"
)

func (s *RewiewRepo) Create(req rs.RewiewReq) (*rs.RewiewRes, error) {
	response := &rs.RewiewRes{}
	err := s.Db.QueryRow(`INSERT INTO rewiews( 
		post_id,
		customer_id,
		rating,
		description
	) values($1, $2, $3, $4)
		returning 
		id,
		post_id,
		customer_id,
		rating,
		description,
		created_at,
		updated_at
		`,
		req.PostId,
		req.CustomerId,
		req.Rating,
		req.Description,
	).Scan(
		&response.Id,
		&response.PostId,
		&response.CustomerId,
		&response.Rating,
		&response.Description,
		&response.CreatedAt,
		&response.UpdatedAt,
	)
	return response, err
}

func (s *RewiewRepo) GetRewiewByPostId(req rs.Id) (*rs.GetRewiewsRes, error) {
	rows, err := s.Db.Query(`select 
	id, 
	post_id, 
	customer_id, 
	rating, 
	description,
	created_at, 
	updated_at
	from rewiews 
	where post_id = $1 and deleted_at is null`, req.Id)
	if err != nil {
		return &rs.GetRewiewsRes{}, err
	}
	defer rows.Close()
	response := &rs.GetRewiewsRes{}

	for rows.Next() {
		temp := &rs.RewiewRes{}
		err = rows.Scan(
			&temp.Id,
			&temp.PostId,
			&temp.CustomerId,
			&temp.Rating,
			&temp.Description,
			&temp.CreatedAt,
			&temp.UpdatedAt,
		)
		if err != nil {
			return &rs.GetRewiewsRes{}, err
		}
		response.Rewiews = append(response.Rewiews, temp)
	}
	err = s.Db.QueryRow(`SELECT ROUND(AVG(rating)) from rewiews 
	   where post_id = $1 and deleted_at is null 
	   group by post_id`, req.Id).Scan(&response.Rating)
	if err != nil {
		return &rs.GetRewiewsRes{}, err
	}
	return response, nil
}

func (s *RewiewRepo) Update(req rs.RewiewRes) (*rs.RewiewRes, error) {
	err := s.Db.QueryRow(`
		UPDATE rewiews SET
		updated_at=NOW(),
		post_id=$1, 
		customer_id=$2,
		rating=$3,
		description=$4
		WHERE id=$5 and deleted_at is null
	`, req.PostId, req.CustomerId, req.Rating, req.Description, req.Id).Err()
	if err != nil {
		return &rs.RewiewRes{}, err
	}
	return &req, nil
}

func (s *RewiewRepo) Delete(req rs.Id) error {
	_, err := s.Db.Exec(`UPDATE rewiews SET deleted_at=NOW() 
	where id=$1 and deleted_at is null`, req.Id)
	return err
}

func (s *RewiewRepo) DeleteByCustomerId(req rs.Id) error {
	_, err := s.Db.Exec(`UPDATE rewiews SET deleted_at=NOW() 
	where customer_id=$1 and deleted_at is null`, req.Id)
	return err
}

func (s *RewiewRepo) DeleteByPostId(req rs.Id) error {
	_, err := s.Db.Exec(`UPDATE rewiews SET deleted_at=NOW() 
	where post_id=$1 and deleted_at is null`, req.Id)
	return err
}

func (s *RewiewRepo) GetTop10Postids() (*rs.TopPosts, error) {
	rows, err := s.Db.Query(`
	SELECT post_id FROM rewiews where deleted_at is null
	GROUP BY post_id 
	ORDER BY SUM(rating) / COUNT(post_id) DESC, COUNT(post_id) DESC
	LIMIT 10
	`)
	if err != nil {
		return &rs.TopPosts{}, err
	}
	defer rows.Close()
	res := &rs.TopPosts{}
	temp := ""
	for rows.Next() {
		err := rows.Scan(&temp)
		if err != nil {
			return &rs.TopPosts{}, err
		}
		res.Ids = append(res.Ids, temp)
	}
	return res, nil
}
