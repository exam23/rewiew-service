package service

import (
	"context"

	pb "gitlab.com/exam23/rewiew-service/genproto/rewiew"
	"gitlab.com/exam23/rewiew-service/pkg/logger"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func (s *RewiewService) Create(ctx context.Context, req *pb.RewiewReq) (*pb.RewiewRes, error) {
	res, err := s.Storage.Rewiew().Create(*req)
	if err != nil {
		s.Logger.Error("Error while creating rewiew", logger.Any("create", err))
		return nil, status.Error(codes.Internal, "Please check your info")
	}
	return res, nil
}

func (s *RewiewService) Update(ctx context.Context, req *pb.RewiewRes) (*pb.RewiewRes, error) {
	res, err := s.Storage.Rewiew().Update(*req)
	if err != nil {
		s.Logger.Error("Error while updating rewiew", logger.Any("update", err))
		return nil, status.Error(codes.Internal, "Please check your info")
	}
	return res, nil
}

func (s *RewiewService) Delete(ctx context.Context, req *pb.Id) (*pb.Empty, error) {
	err := s.Storage.Rewiew().Delete(*req)
	if err != nil {
		s.Logger.Error("Error while delteing rewiew", logger.Any("delete", err))
		return nil, status.Error(codes.Internal, "Please check your info")
	}
	return &pb.Empty{}, nil
}

func (s *RewiewService) GetRewiewByPostId(ctx context.Context, req *pb.Id) (*pb.GetRewiewsRes, error) {
	res, err := s.Storage.Rewiew().GetRewiewByPostId(*req)
	if err != nil {
		s.Logger.Error("Error while getting rewiews by post id", logger.Any("get", err))
		return nil, status.Error(codes.Internal, "Please check your info")
	}
	return res, nil
}

func (s *RewiewService) GetTop10Postids(ctx context.Context, req *pb.Empty) (*pb.TopPosts, error) {
	res, err := s.Storage.Rewiew().GetTop10Postids()
	if err != nil {
		s.Logger.Error("Error while getting top 10 post_ids", logger.Any("get", err))
		return &pb.TopPosts{}, status.Error(codes.Internal, "Please check your info")
	}
	return res, nil
}

func (s *RewiewService) DeleteByCustomerId(ctx context.Context, req *pb.Id) (*pb.Empty, error) {
	err := s.Storage.Rewiew().DeleteByCustomerId(*req)
	if err != nil {
		s.Logger.Error("Error while deleting by customer id", logger.Any("delete", err))
		return &pb.Empty{}, status.Error(codes.Internal, "Please check your info")
	}
	return &pb.Empty{}, nil
}

func (s *RewiewService) DeleteByPostId(ctx context.Context, req *pb.Id) (*pb.Empty, error) {
	err := s.Storage.Rewiew().DeleteByPostId(*req)
	if err != nil {
		s.Logger.Error("Error while deleting by post id", logger.Any("delete", err))
		return &pb.Empty{}, status.Error(codes.Internal, "Please check your info")
	}
	return &pb.Empty{}, nil
}
