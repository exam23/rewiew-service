package service

import (
	"github.com/jmoiron/sqlx"
	"gitlab.com/exam23/rewiew-service/pkg/logger"
	"gitlab.com/exam23/rewiew-service/storage"
)

type RewiewService struct {
	Storage storage.IStorage
	Logger  logger.Logger
}

func NewRewiewService(db *sqlx.DB, l logger.Logger) *RewiewService {
	return &RewiewService{
		Storage: storage.NewStoragePg(db),
		Logger:  l,
	}
}


